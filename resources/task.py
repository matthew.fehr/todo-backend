from flasgger import swag_from
from flask_restful import Resource, reqparse
from models.task import TaskModel

class Task(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('id', type = int)
    parser.add_argument('descrip', type = str)

    @swag_from('../swagger/task/get_task.yaml')
    def get(self, id):
        task = TaskModel (1, "Instalar entorno")
        if task:
            return task.json()

        return {'message': 'No se encuentra Task'}, 404
