class TaskModel():
    id = int
    descrip = str

    def __init__(self, id, descrip):
        self.id = id
        self.descrip = descrip

    def json(self, jsondepth = 0):
        json = {
            'id' : self.id,
            'descrip' : self.descrip
        }

        return json